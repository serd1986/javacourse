package ru.madbrains.javacourse.lesson1;

public class SwitchCase {
    public static void main(String[] args) {
        int mark = 4;

        switch (mark) {
            case 5:
                System.out.println("Отлично");
                break;
            case 4:
                System.out.println("Хорошо");
                break;
            case 3:
                System.out.println("Удовлетворительно");
                break;
            case 2:
            case 1:
                System.out.println("Плохо");
                break;
            default:
                System.out.println("Что-то пошло не так");
                break;
        }
    }
}

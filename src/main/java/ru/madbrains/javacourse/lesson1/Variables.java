package ru.madbrains.javacourse.lesson1;

public class Variables {
    public static void main(String[] args) {
        int i = 1;
        long l = 2L;
        double d = 3.5d;
        boolean b;

        b = true; //присвоение
        i++; //унарная операция
        i = i + 1; //бинарная операция
        i += 1; //комбинированная операция
        b = i > 5; //сравнение

        System.out.println("i = " + i);
        System.out.println("l = " + l);
        System.out.println("d = " + d);
        System.out.println("b = " + b);
    }
}

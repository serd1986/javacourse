package ru.madbrains.javacourse.lesson3;

import ru.madbrains.javacourse.lesson3.employer.Employer;
import ru.madbrains.javacourse.lesson3.employer.Worker;

import java.lang.reflect.Array;

public class EntityManager<T extends Employer> {
    private T[] entities;
    int size;

    @SuppressWarnings("unchecked")
    public EntityManager(int maxSize, Class<T> clazz) {
        this.entities = (T[]) Array.newInstance(clazz, maxSize);
    }

    public void addEntity(T entity) {
        System.out.println(entity.getName() + " is added");
        entities[size] = entity;
        size++;
    }

    public int getSize() {
        return size;
    }

    public T[] getEntities() {
        return entities;
    }
}

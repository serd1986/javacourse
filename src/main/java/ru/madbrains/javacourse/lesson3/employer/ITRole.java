package ru.madbrains.javacourse.lesson3.employer;

public enum ITRole {
    PM,
    QA,
    Developer
}

package ru.madbrains.javacourse.lesson3.employer;

public interface Worker {
    void work();
}

package ru.madbrains.javacourse;

import ru.madbrains.javacourse.homework.part1.MyList;
import ru.madbrains.javacourse.lesson2.Developer;
import ru.madbrains.javacourse.lesson2.Employer;
import ru.madbrains.javacourse.lesson2.MathUtils;

import java.util.Comparator;


public class Program {

    public static Comparator<Employer> SalaryComparator = new Comparator<Employer>() {
        @Override
        public int compare(Employer e1, Employer e2) {
            return e1.getName().compareTo(e2.getName());
        }
    };

    public static void main(String[] args) {
        Employer employer = new Employer();
        employer.work();

        Developer developer = new Developer("Igor Petrov", 26);
        developer.setLanguage("Java");
        developer.work();
        developer.writeCode();

        System.out.println("result = " + MathUtils.factorial(5));
    }
}
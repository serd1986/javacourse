package ru.madbrains.javacourse.homework.part1;

import java.util.Comparator;
import java.util.Optional;
import java.util.Random;

public class MyList<T> implements AdvancedList<T>, AuthorHolder {

    private final int M_SIZE = 10;
    private T[] massive = (T[]) new Object[M_SIZE];
    private int point = 0;

    @Override
    public AdvancedList<T> shuffle() {
        MyList<T> myList = this.cloneList();
        int n = myList.point;
        Random random = new Random();
        random.nextInt();
        for (int i = 0; i < n; i++) {
            int change = i + random.nextInt(n - i);
            swap(myList.massive, i, change);
        }
        return myList;
    }

    @Override
    public AdvancedList<T> sort(Comparator<T> comparator) {
        MyList<T> myList = this.cloneList();
        mSort(myList.massive, myList.point, comparator);
        return myList;
    }

    @Override
    public String author() {
        return "Егоров Сергей";
    }

    @Override
    public void add(T item) {
        resize(point, this.massive.length);
        massive[point++] = item;
    }

    @Override
    public void insert(int index, T item) throws Exception {
        resize(point, this.massive.length);
        System.arraycopy(massive, index , massive, index+1, point - index);
        massive[index] = item;
        point++;
    }

    @Override
    public void remove(int index) throws Exception {
        int countZn=point-index-1;
        System.arraycopy(massive, index + 1, massive, index, countZn);
        massive[--point] = null;
    }

    @Override
    public Optional<T> get(int index) {
        if(index>-1 && index<=point ) {
            return Optional.ofNullable(massive[index]);
        }else{
            return Optional.empty();}
    }

    @Override
    public int size() {
        return point;
    }

    @Override
    public void addAll(SimpleList<T> list) {
        MyList newArray = (MyList)list;
        resize(point + newArray.point, this.massive.length);
        System.arraycopy(newArray.massive, 0, this.massive, (point), newArray.point);
        point+=newArray.point;
    }

    @Override
    public int first(T item) {
        for (int i =0; i<point; i++) {
            if (massive[i].equals(item)){
                return i;
            }
        }
        return -1;
    }

    @Override
    public int last(T item) {
        for (int i =point-1; i>0; i--) {
            if (massive[i].equals(item)){
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T item) {
        for (int i =0; i<point; i++) {
            if (massive[i].equals(item)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return point<1;
    }

    /**
     * Клон обьекта лист
     * @return MyList<T>
     */
    protected MyList<T> cloneList(){
        MyList<T> myList = new MyList<>();
        T[] newArray = (T[]) new Object[massive.length+1];
        newArray=massive.clone();

        myList.point=this.point;
        myList.massive=newArray;
        return myList;
    }

    /**
     * Метод сортирует массив.
     * @param massive массив джинериков.
     * @param point кол-во обьектов в массиве a.
     * @param comparator Comparator <потребитель>
     * @return Ничего.
     */
    private static void mSort(Object[] massive, int point, Comparator comparator) {
        for (int i=0; i<point; i++)
            for (int j=i; j>0 && comparator.compare(massive[j-1], massive[j])>0; j--)
                swap(massive, j, j-1);
        return;
    }

    /**
     * Метод меняет индекс объекта в массиве.
     * @param arr массив.
     * @param i исходный индекс.
     * @param change на какой изменить.
     * @return Ничего.
     */
    private static void swap(Object[] arr, int i, int change) {
        Object temp = arr[i];
        arr[i] = arr[change];
        arr[change] = temp;
    }

    /**
     * Метод увеличивает размер массива если это требуется.
     */
    private void resize(int point_, int _length){
        if ((point_+1)>=_length) {
            T[] newArray = (T[]) new Object[(_length * 3) / 2 + 1];
            System.arraycopy(massive, 0, newArray, 0, point);
            massive = newArray;
        }
    }
}